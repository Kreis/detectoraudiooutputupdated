#include <Windows.h>
#include <mmdeviceapi.h>
#include <endpointvolume.h>
#include <functional>

// The notification client class
class AudioNotificationClient : public IMMNotificationClient {
public:
    // callback will be called when default audio output changed
    AudioNotificationClient(std::function<void()> callback) {
        this->callback = callback;
        Start();
    }

    ~AudioNotificationClient() {
        Close();
    }

    // Why an update?
    // when default audio output changed triggers several events
    // like 4, so when that happens we'll wait for a delay before
    // triger a single callback notifying single modification
    void update(float deltatime) {
      if (countEvent == 0) {
        currentTime = 0.0;
        return;
      }

      currentTime += deltatime;
      if (currentTime < delay) {
        return;
      }

      countEvent = 0;
      currentTime = 0.0;
      callback();
    }

    bool Start() {
        // Initialize the COM library for the current thread
        HRESULT ihr = CoInitialize(NULL);

        if (SUCCEEDED(ihr)) {
            // Create the device enumerator
            IMMDeviceEnumerator* pEnumerator;
            HRESULT hr = CoCreateInstance(__uuidof(MMDeviceEnumerator), NULL, CLSCTX_ALL, __uuidof(IMMDeviceEnumerator), (void**)&pEnumerator);
            if (SUCCEEDED(hr)) {
                // Register for device change notifications
                hr = pEnumerator->RegisterEndpointNotificationCallback(this);
                m_pEnumerator = pEnumerator;

                return true;
            }

            CoUninitialize();
        }

        return false;
    }

    void Close() {
        // Unregister the device enumerator
        if (m_pEnumerator) {
            m_pEnumerator->UnregisterEndpointNotificationCallback(this);
            m_pEnumerator->Release();
        }

        // Uninitialize the COM library for the current thread
        CoUninitialize();
    }

    // IUnknown methods
    STDMETHOD(QueryInterface)(REFIID riid, void** ppvObject) {
        if (riid == IID_IUnknown || riid == __uuidof(IMMNotificationClient)) {
            *ppvObject = static_cast<IMMNotificationClient*>(this);
            AddRef();
            return S_OK;
        }
        return E_NOINTERFACE;
    }

    ULONG STDMETHODCALLTYPE AddRef() {
        return InterlockedIncrement(&m_cRef);
    }

    ULONG STDMETHODCALLTYPE Release() {
        ULONG ulRef = InterlockedDecrement(&m_cRef);
        if (0 == ulRef) {
            delete this;
        }
        return ulRef;
    }

    // IMMNotificationClient methods
    STDMETHOD(OnDefaultDeviceChanged)(EDataFlow flow, ERole role, LPCWSTR pwstrDeviceId) {
        // Default audio device has been changed.
        countEvent++;
        
        return S_OK;
    }

    STDMETHOD(OnDeviceAdded)(LPCWSTR pwstrDeviceId) {
        // A new audio device has been added.
        return S_OK;
    }

    STDMETHOD(OnDeviceRemoved)(LPCWSTR pwstrDeviceId) {
        // An audio device has been removed.
        return S_OK;
    }

    STDMETHOD(OnDeviceStateChanged)(LPCWSTR pwstrDeviceId, DWORD dwNewState) {
        // The state of an audio device has changed.
        return S_OK;
    }

    STDMETHOD(OnPropertyValueChanged)(LPCWSTR pwstrDeviceId, const PROPERTYKEY key) {
        // A property value of an audio device has changed.
        return S_OK;
    }

private:
    LONG m_cRef;
    IMMDeviceEnumerator* m_pEnumerator;

    std::function<void()> callback;
    int32_t countEvent = 0;
    float delay = 2.5;
    float currentTime = 0.0;
};


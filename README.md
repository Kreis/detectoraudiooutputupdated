# C++ Simple Callback for default audio ouput update

This is a simple header that is waiting for an update in the default audio ouput device (like connect headphones or return to speakers on the fly).

For example using SFML you cannot (for the moment) know when this happen, so with this callback you can re-create Audio and Music resources to take the new default audio output.

NOTE: it is only thought for Win x64

> reference for the main idea:
https://stackoverflow.com/questions/74965245/how-to-detect-default-audio-output-device-change-in-windows


### How to compile it
```
g++ main.cpp -lole32 -loleaut32 -luuid
```

### How to use it
```c++
#include "AudioNotificationClient.h"

// initialize object with a call back <void()>
AudioNotificationClient anc(callback);

// update with deltatime in seconds
anc.update(0.166)

// eventually when default output device is changed
// callback will be triggered
```

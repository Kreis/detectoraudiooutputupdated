#include <iostream>
#include <functional>
#include <chrono>
#include <thread>
#include "AudioNotificationClient.h"

int main() {

  auto callback = []() {
    std::cout << "Default Audio Output modified" << std::endl;
  };

  AudioNotificationClient anc(callback);

  for (int i = 0; i < 500; i++) {
    std::cout << "iteration: " << (i + 1) << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(200));

    anc.update(0.2);
  } 

  std::cout << "finish now" << std::endl;

  return 0;
}
